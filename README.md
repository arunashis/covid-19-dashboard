# COVID-19 Dashboard

A COVID-19 dashboard depicting various data points for Australia and Worldwide.
The dashboard for Australia is created using D3.js while the Global dashboard is developed using Tableau v12.

## Global Dashboard

![Global Dashboard](./img/Dashboard_Global.png)

### Data Source

Positive Cases time series: https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv <br>
Deaths time series: https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv <br>
Country population: https://www.kaggle.com/tanuprabhu/population-by-country-2020

The python notebook used to pre-process the above datasets is [here](./data_processing/cssejhu_covid_global_data_transform.ipynb) in this repo.

### Dashboard description and how to use

This dashboard has been created in *Tableau Desktop*. Then the dashboard has been published to [Tableau Public Server](https://public.tableau.com) and 
then it has been shared in this webpage using HTML/Javacript provided by Tableau.

The ***Choropleth*** map shows either "Actual Value" or "Per million" value of either "Positive Cases" or "Deaths" for all countries. Use two right-most
dropdowns to change those parameters. The ***Horizontal bar chart*** shows the same data as ***Choropleth*** map. There are a few ways to filter these two charts -

* Select one or more continent(s) from the "Continent" dropdown menu to see a comparison within that continent(s),
* Use the "Range filter" dropdown to select one or more particular value range(s) to see a comparison within that range(s).

The left-most column of the dashboard contains two time series charts. They appear only when you select a specific country from either the 
***Choropleth*** map or ***Horizontal bar chart***. The top time series chart shows **Positive Cases** and the bottom one shows **Deaths**. Both 
the charts show either **Daily** or **Cumulative** value depending on what you choose from left-most dropdown menu. You can also zoom into the 
charts by modifying the "Date Range" selector.

The "Color Range" shows the color legend on the ***Choropleth*** map.

### Data pre-precossing

Substantial amount of data pre-processing has been done for this dashboard. Python (*pandas* library) and Jupyter notebook have been used for pre-processing. 
Following is a short descriptive step-by-step list of the pre-processing tasks done:

1. Country name for certain countries have been rectified so that they can be recognized by Tableau and mapped to *Country population* record.
2. In the datasets, certain locations are shown as a province/state of a country. For example, Greenland is shown as province/state under Denmark. Later We will 
   be merging all province data into their respective countries (e.g., for China, Australia, etc.), however we do not want to merge geophraphically far away or separate 
   locations into their respective country, mainly because those locations could be in another continent or they counld have very different COVID-19 data pattarn
   compared to the country they belong to. Therefore, for such locations, we copied the 'Province/State' value into the 'Country/Region' field.
3. Since, data for certain countries (e.g., Australia, China, etc.) are segregated into their states/provinces, we summed up all those states' data 
   into the approprite country.
4. For each country, it's continent has been added to the *continent* column using Python's *pycountry_convert* library.
5. 'Lat' & 'Long' columns have been dropped sine Tableau generates Lat-Long value based on location name.
6. Source dataset contains time series data and each date are represented as a column. This is problematic for visualization in Tableau. So, the dataset has been 
   reformatted so that each date column becomes a row for each individual country.
7. Step 1 -7 have been applied for both *Positive Cases* and *Deaths* dataframe (*pandas* loads each csv file into a *DataFrame* object).
8. By combiling *Positive Cases* and *Deaths* dataframe using an inner join, we create a combined dataframe.
9. Both the source datasets contain only cumulative values for each date. Therefore, we need to calculate the daily value for each date and each country.
    We sorted the combined dataframe by Country name and Date. Then, we added two new columns for 'Daily Positive Cases' and 'Daily Deaths'. Finally, we
    looped through the dataframe to fill in those two columns for each date and country.
10. The final dataframe has been exported to a csv file, which becomes the data source for Tableau.

### Creation of the dashboard

The design and idea of the dashboard are inspired by [Tableau COVID-19 Data Hub](https://www.tableau.com/covid-19-coronavirus-data-resources). However, the dashboard has been created from scratch instead of directly copying anything from any of those examples. We used the following Tableau tools (among others) for the Dashboard:

* Choropleth map,
* Bar chart,
* Line chart,
* Calculated fields (Analysis -> Create Calculated Filed) for dynamically updating chart, text labels and to create the date range filter,
* Actions (Worksheet -> Actions) to update one chart based on the value selected in another chart/map.

## The Dashboard for Australia

![Dashboard for Australia](./img/Dashboard_Australia.png)

### Data source

COVID-19 state-wise datasets for Australia are taken from here: <br>

This describes the project and the data: https://www.covid19data.com.au/data-notes <br>
This has the data: https://github.com/M3IT/COVID-19_Data

### Dashboard description and how to use

The bar chart on the left side shows a comparison among the Australian states. The data for this comparison can chosen from the top left 
dropdown menu. To get the actual values of positive cases, deaths and vaccination for an individual state, you need to bring 
the mouse pointer on top of the respective bar.

The line chart on the right side shows a time series for an individual state. The state and type of the time series (daily or cumulative)
can be chosesn from the middle and right-most dropdown menu respectively. You can zoom into the chart by selecting an area in the chart and
you can zoom out by double-clicking on the chart.

### Data pre-precossing

The data available in the aforementioned site is in csv format and it's a time series data for each individual state. For this particular project,
we do not want any data loading delay when the page loads. So, we converted the csv data into two separate json datasets - one containing the 
latest cumulative values for each state, the other containing a time series for each individual state. We used one of many available online sites
to convert from csv to minified json.

### Creation of the dashboard

To create the charts, we used D3.js. There are many online sites to get a hang of D3. We used [D3.js Graph Gallery](https://www.d3-graph-gallery.com/),
as it provides an extensive examples for many different types of charts and graphs. Aside D3, we used HTML, CSS and javascript.
